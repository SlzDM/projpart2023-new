---
layout: post
title: Projet 8 - Les ânes de l’Arche 
date: 2021-02-01
description: Petite description du projet 1
img: projet1.png # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [projet]
---

Qolline, Lorette, Nasser et Hansel sont des ânes. Qolline et Lorette sont arrivées en automne 2020 à Salzinnes. Nasser et Hansel les ont rejoint en cette fin d’hiver 2021. La petite troupe enchante déjà les personnes qui vivent à la communauté de l’Arche et les enfants du quartier qui peuvent venir leur rendre visite librement.

Le projet, porté par une association de fait salzinnoise est de permettre aux personnes différentes intellectuellement habitant à l'Arche d'être en relation avec ces animaux calmes et attachants.  Leur présence permet aussi de créer du lien avec les habitants de Salzinnes au travers d’activités avec les écoles proches, de la participation à des festivités dans le quartier et de balades aux alentours, dans les rues avoisinantes ou à la citadelle.

L’installation définitive des ânes demande encore quelques aménagements. Le subside permettra notamment de leur préparer un pré pour l’été sur le terrain de l’Arche (clôtures, semis, haies, pré fleuri,...), et de mettre en place un espace d’accueil extérieur pour les petits et les grands afin de développer pleinement les interactions entre les participants et les ânes.

## Contacts :

Pour l'association de fait "Les ânes de l'Arche" :
- Maude Verhulst (0495/25 20 86  ou [maude.verhulst@gmail.com](mailto:maude.verhulst@gmail.com))
- Yves Deltombe (0486/56 09 52 ou [yves.deltombe@gmail.com](mailto:yves.deltombe@gmail.com))


![Avec d'autres photos]({{site.baseurl}}/assets/img/50558384597_fdfded8636_k.jpg)
![Avec d'autres photos]({{site.baseurl}}/assets/img/50591094992_439e32d15a_k.jpg)
![Avec d'autres photos]({{site.baseurl}}/assets/img/50645920477_a9713eedf2_o.jpg)
![Avec d'autres photos]({{site.baseurl}}/assets/img/DSC7183-3-3.jpg)
