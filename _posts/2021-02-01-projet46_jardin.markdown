---
layout: post
title: "Projet 46 - Au coeur de Salzinnes : jardin partagé"
date: 2021-02-01
description: Petite description du projet 1
img: projet2.JPG # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [projet]
---

Au cœur de Salzinnes, ce **projet intergénérationnel** vise à rendre vivant et plus attractif le jardin, qui se trouve rue Julien Colson, 39 à 5000 Namur. Grâce à ce projet d’aménagement, cet espace aura pour vocation d’accueillir **plusieurs zones d’activités partagées** : des carrés potagers afin de cultiver aromates et légumes de saison, une aire de repos et de jeux, ainsi qu’un espace dédié aux activités physiques adaptées.

Cette initiative rassemble **3 acteurs du quartier de Salzinnes** : la crèche Notre dame avec les enfants et leurs parents, les citoyens et membres du quartier de Salzinnes, et l’Espace Bien-être **l’Essentiel** dédié aux personnes confrontées au cancer et à leurs proches. 

Enfants, parents, citoyens et patients feront vivre ce lieu et pourront se ressourcer dans ce nouvel espace aménagé. L’objectif est de créer des « **synergies** » entre ces acteurs en échangeant et en partageant leurs idées autour de la création, l’entretien ou l’organisation de ces différentes activités.
	
**L’aménagement de ce jardin partagé** est un projet que nous avons depuis mars 2019. Il n’a pu voir le jour faute de financement à ce jour. Nous comptons vivement sur votre soutien pour nous aider à animer ce beau jardin ! D’avance, un grand merci

## Contacts

Personnes de contact pour l’organisation et le suivi du projet :
- **Docteur Bernard Willemart** - Chef du service d’oncologie, hématologie, médecine nucléaire et radiothérapie (SORMN) du CHU UCL Namur site Sainte Elisabeth
  - Tel : 081/720 772 
  - [Bernard.willemart@uclouvain.be](mailto:Bernard.willemart@uclouvain.be)

- **Virginie Fontaine** - Coordinatrice de l’Espace Bien-être l’Essentiel
  - Tel : 081/720 523 – 0473/51 42 37 
  - [Virginie.fontaine@uclouvain.be](mailto:Virginie.fontaine@uclouvain.be)

Vous souhaitez en savoir plus sur l’Espace Bien-être l’Essentiel ?

[www.lessentiel-namur.be](http://www.lessentiel-namur.be)


## Images

![Photos Jardin]({{site.baseurl}}/assets/img/projet2_photo2.JPG)

![Photos Jardin]({{site.baseurl}}/assets/img/projet2_logo1.jpg) 

![Photos Jardin]({{site.baseurl}}/assets/img/projet2_logo2.png)

