---
layout: post
title: Projet 23 - Plan Directeur pour Salzinnes 
date: 2021-02-01
description: Petite description du projet 3
img: projet3.jpg # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [projet]
---


Vous vous souciez de votre environnement immédiat ? La qualité de l'air vous inquiète ? La disparition des espaces naturels vous chagrine ? Les divers chantiers et projets urbanistiques vous interpellent ? L'insécurité routière vous pèse ? Le stationnement est un casse-tête ? Les commerces sont difficilement accessibles ? ...

Qui est mieux placé que vous, Salzinnois.e, pour construire un projet durable pour Salzinnes ?
Unissons nos forces, entrons dans une démarche où nos vécus, nos connaissances et nos souhaits pour le faubourg comptent !

Apportons nos contributions pour faire de Salzinnes un quartier durable : plus de lieux de convivialité, de l'espace vert pour tou.te.s, un équilibre habitat/infrastructure, davantage de mixité sociale, etc.

Construisons ensemble un avenir pour la piscine, la place Ryckmans, les derniers espaces verts avenue du Val St Georges, un meilleur partage de l'espace public, des voiries apaisées et plus sûres pour tous, une place Godin conviviale, ...

Ce plan directeur élaboré avec le soutien de l'administration communale et d'experts professionnels, servira d'outil de référence pour les décisions futures concernant notre faubourg.

Des exemples concrets (et locaux) de schémas directeurs sont présentés sur [le site de la ville de Namur, pour les quartiers de Bomel et sud-est de l’agglomération](https://www.namur.be/fr/ma-ville/administration/services-communaux/service-technique-du-developpement-territorial/outils-de-planification/schemas-directeurs-damenagement-durable).

Pour référence, voici le texte officiel qui décrira notre projet sur le site de vote de la Ville :

	Le Collectif Salzinnes-Demain vise à mobiliser les forces vives de Salzinnes autour d’une démarche citoyenne participative et constructive.

	Le projet ambitionne la réalisation d'un outil urbanistique (Plan Directeur) en vue de planifier le développement futur de Salzinnes comme quartier durable : un environnement sain, une qualité de vie pour tou.te.s,un équilibre entre habitat/infrastructures/espace vert.

	Ce Plan Directeur constitue un outil qui favorise une étude prospective, globale et co-construite sur l'avenir du faubourg intégrant les différents mouvements immobiliers, l'impact d'implantations nouvelles et les problèmes liés à la mobilité.

	Ce travail doit pouvoir s'appuyer sur l'expertise des citoyen.ne.s (leurs vécus,besoins et souhaits).

	Il nécessite une ouverture constructive à l'ensemble des intervenants publics et privés.

	Ce travail est enrichi par les outils d'analyses de professionnel.le.s de l'aménagement du territoire et des services compétents de la ville de Namur.

# Contact

- **Collectif Salzinnes-Demain asbl**
  - [info@salzinnes-demain.org](mailto:info@salzinnes-demain.org)

# Liens utiles
- Le site web du collectif Salzinnes Demain
[https://salzinnes-demain.org/](https://salzinnes-demain.org)

# Images

![Photos]({{site.baseurl}}/assets/img/projet3_photo2.jpg)
![Photos]({{site.baseurl}}/assets/img/CSD_Vue_générale_Salzinnes.jpg)
![Photos]({{site.baseurl}}/assets/img/CSD_Réunion_publique1.JPG)
![Photos]({{site.baseurl}}/assets/img/CSD_Réunion_publique2.JPG)
![Photos]({{site.baseurl}}/assets/img/Marc_Ronvaux_Salzinnes_NR043.jpg)
![Photos]({{site.baseurl}}/assets/img/BP2020-17_Visuel_projet_Salzinnes-Demain.jpg)
